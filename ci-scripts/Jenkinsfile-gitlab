#!/bin/groovy
/*
 * Licensed to the OpenAirInterface (OAI) Software Alliance under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The OpenAirInterface Software Alliance licenses this file to You under
 * the OAI Public License, Version 1.1  (the "License"); you may not use this file
 * except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.openairinterface.org/?page_id=698
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 * For more information about the OpenAirInterface (OAI) Software Alliance:
 *      contact@openairinterface.org
 */

// Abstraction function to send social media messages:
// like on Slack or Mattermost
def sendSocialMediaMessage(pipeChannel, pipeColor, pipeMessage) {
    if (params.pipelineUsesSlack != null) {
        if (params.pipelineUsesSlack) {
            slackSend channel: pipeChannel, color: pipeColor, message: pipeMessage
        }
    }
}

def doRedHatBuild = false
def doFlexranCtrlTest = false

pipeline {
    agent any
    
    options {
        disableConcurrentBuilds()
        timestamps()
        /* gitLabConnection('OAI GitLab') */
        gitlabBuilds(builds: ["Build basic-sim", "Build phy-sim", "Analysis with cppcheck", "Test phy-sim", "Test basic-sim", "Test L2-sim"])
        ansiColor('xterm')
    }
    
    stages {
    
        stage ("Verify Guidelines") {
            steps {
                echo "Git URL         is ${GIT_URL}"
                echo "GitLab Act      is ${env.gitlabActionType}"
                script {
                    if ("MERGE".equals(env.gitlabActionType)) {
                        // GitLab-Jenkins plugin integration is lacking to perform the merge by itself
                        // Doing it manually --> it may have merge conflicts
                        sh "./ci-scripts/doGitLabMerge.sh --src-branch ${env.gitlabSourceBranch} --src-commit ${env.gitlabMergeRequestLastCommit} --target-branch ${env.gitlabTargetBranch} --target-commit ${GIT_COMMIT}"
                        sh "zip -r -qq localZip.zip ."

                        // Running astyle options on the list of modified files by the merge request
                        // For the moment, there is no fail criteria. Just a notification of number of files that do not follow
                        sh "./ci-scripts/checkCodingFormattingRules.sh --src-branch ${env.gitlabSourceBranch} --target-branch ${env.gitlabTargetBranch}"
                        def res=readFile('./oai_rules_result.txt').trim();
                        if ("0".equals(res)) {
                            def message = "OAI " + JOB_NAME + " build (" + BUILD_ID + "): All Changed files in Merge Request follow OAI Formatting Rules"
                            addGitLabMRComment comment: message
                        } else {
                            def message = "OAI " + JOB_NAME + " build (" + BUILD_ID + "): Some Changed files in Merge Request DO NOT follow OAI Formatting Rules"
                            addGitLabMRComment comment: message
                        }
                    } else {
                        echo "Git Branch      is ${GIT_BRANCH}"
                        echo "Git Commit      is ${GIT_COMMIT}"
                        sh "git log -n1 --pretty=format:\"%s\" > .git/CI_COMMIT_MSG"

                        sh "zip -r -qq localZip.zip ."
                        // Running astyle options on all C/H files in the repository
                        // For the moment, there is no fail criteria. Just a notification of number of files that do not follow
                        sh "./ci-scripts/checkCodingFormattingRules.sh"
                    }
                    if (doFlexranCtrlTest) {
                        sh "mkdir flexran"
                        dir ('flexran') {
                            withCredentials([
                                [$class: 'UsernamePasswordMultiBinding', credentialsId: "${params.FlexRanRtcGitLabRepository_Credentials}", usernameVariable: 'git_username', passwordVariable: 'git_password']
                                ]) {
                                sh "git clone https://${git_username}:${git_password}@gitlab.eurecom.fr/flexran/flexran-rtc.git . > ../git_clone.log 2>&1"
                            }
                            sh "sed -i -e 's#add-apt-repository.*cleishm.*neo4j#add-apt-repository ppa:cleishm/neo4j -y#' -e 's#libneo4j-client-dev#libneo4j-client-dev -y#' tools/install_dependencies"
                            sh "zip -r -qq flexran.zip ."
                        }
                    }
                }
            }
            post {
                failure {
                    script {
                        def message = "OAI " + JOB_NAME + " build (" + BUILD_ID + "): Merge Conflicts -- Cannot perform CI"
                        addGitLabMRComment comment: message
                    }
                }
            }
        }

        stage ("Start VM -- basic-sim") {
            steps {
                echo 'VM BASIC SIM ...'
                timeout (time: 5, unit: 'MINUTES') {
                    sh "./ci-scripts/oai-ci-vm-tool build --workspace $WORKSPACE --variant basic-sim --job-name ${JOB_NAME} --build-id ${BUILD_ID} --daemon"
                }
            }
        }
        
        stage ("Start VM -- L2-Sim") {
            steps {
                echo 'VM L2 SIM ...'
                timeout (time: 5, unit: 'MINUTES') {
                    sh "./ci-scripts/oai-ci-vm-tool build --workspace $WORKSPACE --variant l2-sim --job-name ${JOB_NAME} --build-id ${BUILD_ID} --daemon"
                }
            }
        }

        stage ("Start VM -- phy-sim") {
            steps {
                timeout (time: 5, unit: 'MINUTES') {
                    sh "./ci-scripts/oai-ci-vm-tool build --workspace $WORKSPACE --variant phy-sim --job-name ${JOB_NAME} --build-id ${BUILD_ID} --daemon"
                }
            }
        }

        stage ("Start VM -- cppcheck") {
            steps {
                timeout (time: 5, unit: 'MINUTES') {
                    sh "./ci-scripts/oai-ci-vm-tool build --workspace $WORKSPACE --variant cppcheck --job-name ${JOB_NAME} --build-id ${BUILD_ID} --daemon"
                }
            }
        }

        stage ("Variant Builds") {
            parallel {
                stage ("Analysis with cppcheck") {
                    steps {
                        gitlabCommitStatus(name: "Analysis with cppcheck") {
                            timeout (time: 20, unit: 'MINUTES') {
                                sh "./ci-scripts/oai-ci-vm-tool wait --workspace $WORKSPACE --variant cppcheck --job-name ${JOB_NAME} --build-id ${BUILD_ID}"
                            }
                        }
                    }
                }
                stage ("Build basic simulator") {
                    steps {
                        gitlabCommitStatus(name: "Build basic-sim") {
                            timeout (time: 20, unit: 'MINUTES') {
                                sh "./ci-scripts/oai-ci-vm-tool wait --workspace $WORKSPACE --variant basic-sim --job-name ${JOB_NAME} --build-id ${BUILD_ID} --keep-vm-alive"
                            }
                        }
                    }
                }
                stage ("Build L2-Simulator-eNB") {
                    steps {
                        //gitlabCommitStatus(name: "Build UE-ethernet") {
                            timeout (time: 20, unit: 'MINUTES') {
                                sh "./ci-scripts/oai-ci-vm-tool wait --workspace $WORKSPACE --variant l2-sim --job-name ${JOB_NAME} --build-id ${BUILD_ID} --keep-vm-alive"
                            }
                        //}
                    }
                }
                stage ("Build physical simulators") {
                    steps {
                        gitlabCommitStatus(name: "Build phy-sim") {
                            timeout (time: 20, unit: 'MINUTES') {
                               sh "./ci-scripts/oai-ci-vm-tool wait --workspace $WORKSPACE --variant phy-sim --job-name ${JOB_NAME} --build-id ${BUILD_ID} --keep-vm-alive"
                            }
                        }
                    }
                }
            }
            post {
                always {
                    script {
                        dir ('archives') {
                            sh "zip -r -qq vm_build_logs.zip basic_sim enb_usrp phy_sim cppcheck enb_eth ue_eth red_hat"
                        }
                        if(fileExists('archives/vm_build_logs.zip')) {
                            archiveArtifacts artifacts: 'archives/vm_build_logs.zip'
                        }
                        if ("MERGE".equals(env.gitlabActionType)) {
                            sh "./ci-scripts/oai-ci-vm-tool report-build --workspace $WORKSPACE --git-url ${GIT_URL} --job-name ${JOB_NAME} --build-id ${BUILD_ID} --trigger merge-request --src-branch ${env.gitlabSourceBranch} --src-commit ${env.gitlabMergeRequestLastCommit} --target-branch ${env.gitlabTargetBranch} --target-commit ${GIT_COMMIT}"
                            sh "./ci-scripts/checkAddedWarnings.sh --src-branch ${env.gitlabSourceBranch} --target-branch ${env.gitlabTargetBranch}"
                            def res=readFile('./oai_warning_files.txt').trim();
                            if ("0".equals(res)) {
                                echo "No issues w/ warnings/errors in this merge request"
                            } else {
                                def fileList=readFile('./oai_warning_files_list.txt').trim();
                                def message = "OAI " + JOB_NAME + " build (" + BUILD_ID + "): Some modified files in Merge Request MAY have INTRODUCED WARNINGS (" + fileList + ")"
                                addGitLabMRComment comment: message
                            }
                        } else {
                            sh "./ci-scripts/oai-ci-vm-tool report-build --workspace $WORKSPACE --git-url ${GIT_URL} --job-name ${JOB_NAME} --build-id ${BUILD_ID} --trigger push --branch ${GIT_BRANCH} --commit ${GIT_COMMIT}"
                        }
                        if(fileExists('build_results.html')) {
                            sh "sed -i -e 's#Build-ID: ${BUILD_ID}#Build-ID: <a href=\"${BUILD_URL}\">${BUILD_ID}</a>#' -e 's#TEMPLATE_BUILD_TIME#${JOB_TIMESTAMP}#' build_results.html"
                            archiveArtifacts artifacts: 'build_results.html'
                        }
                    }
                }
            }
        }

        stage ("Variant Tests") {
            parallel {
                stage ("VM-based tests") {
                    stages {
                        stage ("Test physical simulators") {
                            steps {
                                script {
                                    gitlabCommitStatus(name: "Test phy-sim") {
                                        timeout (time: 20, unit: 'MINUTES') {
                                            try {
                                                sh "./ci-scripts/oai-ci-vm-tool test --workspace $WORKSPACE --variant phy-sim --job-name ${JOB_NAME} --build-id ${BUILD_ID}"
                                            } catch (Exception e) {
                                              currentBuild.result = 'FAILURE'
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        stage ("Test basic simulator") {
                            steps {
                                script {
                                    gitlabCommitStatus(name: "Test basic-sim") {
                                        timeout (time: 30, unit: 'MINUTES') {
                                            try {
                                                sh "./ci-scripts/oai-ci-vm-tool test --workspace $WORKSPACE --variant basic-sim --job-name ${JOB_NAME} --build-id ${BUILD_ID}"
                                            } catch (Exception e) {
                                              currentBuild.result = 'FAILURE'
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        stage ("Test L2 simulator") {
                            steps {
                                script {
                                    gitlabCommitStatus(name: "Test L2-sim") {
                                        timeout (time: 30, unit: 'MINUTES') {
                                            try {
                                                sh "./ci-scripts/oai-ci-vm-tool test --workspace $WORKSPACE --variant l2-sim --job-name ${JOB_NAME} --build-id ${BUILD_ID}"
                                            } catch (Exception e) {
                                                currentBuild.result = 'FAILURE'
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        stage ("Test L1 simulator") {
                            steps {
                                script {
                                    gitlabCommitStatus(name: "Test L1-sim") {
                                        timeout (time: 30, unit: 'MINUTES') {
                                            try {
                                                sh "./ci-scripts/oai-ci-vm-tool test --workspace $WORKSPACE --variant enb-ethernet --job-name ${JOB_NAME} --build-id ${BUILD_ID}"
                                            } catch (Exception e) {
                                                currentBuild.result = 'FAILURE'
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            post {
                always {
                    script {
                        dir ('archives') {
                            sh "if [ -d basic_sim/test ] || [ -d phy_sim/test ] || [ -d l2_sim/test ]; then zip -r -qq vm_tests_logs.zip */test ; fi"
                        }
                        if(fileExists('archives/vm_tests_logs.zip')) {
                            archiveArtifacts artifacts: 'archives/vm_tests_logs.zip'
                            if ("MERGE".equals(env.gitlabActionType)) {
                                sh "./ci-scripts/oai-ci-vm-tool report-test --workspace $WORKSPACE --git-url ${GIT_URL} --job-name ${JOB_NAME} --build-id ${BUILD_ID} --trigger merge-request --src-branch ${env.gitlabSourceBranch} --src-commit ${env.gitlabMergeRequestLastCommit} --target-branch ${env.gitlabTargetBranch} --target-commit ${GIT_COMMIT}"
                            } else {
                                sh "./ci-scripts/oai-ci-vm-tool report-test --workspace $WORKSPACE --git-url ${GIT_URL} --job-name ${JOB_NAME} --build-id ${BUILD_ID} --trigger push --branch ${GIT_BRANCH} --commit ${GIT_COMMIT}"
                            }
                            if(fileExists('test_simulator_results.html')) {
                                sh "sed -i -e 's#Build-ID: ${BUILD_ID}#Build-ID: <a href=\"${BUILD_URL}\">${BUILD_ID}</a>#' -e 's#TEMPLATE_BUILD_TIME#${JOB_TIMESTAMP}#' test_simulator_results.html"
                                archiveArtifacts artifacts: 'test_simulator_results.html'
                            }
                        }
                    }
                }
            }
        }
        stage ("Destroy all Virtual Machines") {
            steps {
                sh "./ci-scripts/oai-ci-vm-tool destroy --job-name ${JOB_NAME} --build-id ${BUILD_ID}"
            }
        }
    }
    post {
        always {
            script {
                // Stage destroy may not be run if error in previous stage
                sh "./ci-scripts/oai-ci-vm-tool destroy --job-name ${JOB_NAME} --build-id ${BUILD_ID}"
                emailext attachmentsPattern: '*results*.html',
                     body: '''Hi,
Here are attached HTML report files for $PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!

Regards,
OAI CI Team''',
                     replyTo: 'no-reply@openairinterface.org',
                     subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!',
                     to: env.gitlabUserEmail

                if (fileExists('.git/CI_COMMIT_MSG')) {
                    sh "rm -f .git/CI_COMMIT_MSG"
                }
            }
        }
        success {
            script {
                def message = "OAI " + JOB_NAME + " build (" + BUILD_ID + "): passed (" + BUILD_URL + ")"
                if ("MERGE".equals(env.gitlabActionType)) {
                    echo "This is a MERGE event"
                    addGitLabMRComment comment: message
                    def message2 = "OAI " + JOB_NAME + " build (" + BUILD_ID + "): passed (" + BUILD_URL + ") -- MergeRequest #" + env.gitlabMergeRequestIid + " (" + env.gitlabMergeRequestTitle + ")"
                    sendSocialMediaMessage('ci-enb', 'good', message2)
                } else {
                    sendSocialMediaMessage('ci-enb', 'good', message)
                }
            }
        }
        failure {
            script {
                def message = "OAI " + JOB_NAME + " build (" + BUILD_ID + "): failed (" + BUILD_URL + ")"
                if ("MERGE".equals(env.gitlabActionType)) {
                    echo "This is a MERGE event"
                    addGitLabMRComment comment: message
                    def message2 = "OAI " + JOB_NAME + " build (" + BUILD_ID + "): failed (" + BUILD_URL + ") -- MergeRequest #" + env.gitlabMergeRequestIid + " (" + env.gitlabMergeRequestTitle + ")"
                    sendSocialMediaMessage('ci-enb', 'danger', message2)
                } else {
                    sendSocialMediaMessage('ci-enb', 'danger', message)
                }
            }
        }
    }
}
